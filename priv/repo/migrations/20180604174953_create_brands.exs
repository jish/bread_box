defmodule BreadBox.Repo.Migrations.CreateBrands do
  use Ecto.Migration

  def change do
    create table(:brands) do
      add :parent_id, :integer
      add :name, :string

      timestamps()
    end

  end
end
