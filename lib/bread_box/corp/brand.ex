defmodule BreadBox.Corp.Brand do
  use Ecto.Schema
  import Ecto.Changeset


  schema "brands" do
    field :name, :string
    field :parent_id, :integer

    timestamps()
  end

  @doc false
  def changeset(brand, attrs) do
    brand
    |> cast(attrs, [:parent_id, :name])
    |> validate_required([:name])
  end
end
