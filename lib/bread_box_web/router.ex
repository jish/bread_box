defmodule BreadBoxWeb.Router do
  use BreadBoxWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", BreadBoxWeb do
    pipe_through :browser # Use the default browser stack

    resources "/brands", BrandController

    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  # scope "/api", BreadBoxWeb do
  #   pipe_through :api
  # end
end
