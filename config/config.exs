# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :bread_box,
  ecto_repos: [BreadBox.Repo]

# Configures the endpoint
config :bread_box, BreadBoxWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "PSUpENTkGQZQ1aSW70NCUUKIKhBf8x4In6Oplz6g6V9kODWC8Q7fmHB3zEG6zlCV",
  render_errors: [view: BreadBoxWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: BreadBox.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
