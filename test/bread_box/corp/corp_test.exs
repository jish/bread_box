defmodule BreadBox.CorpTest do
  use BreadBox.DataCase

  alias BreadBox.Corp

  describe "brands" do
    alias BreadBox.Corp.Brand

    @valid_attrs %{name: "some name", parent_id: 42}
    @update_attrs %{name: "some updated name", parent_id: 43}
    @invalid_attrs %{name: nil, parent_id: nil}

    def brand_fixture(attrs \\ %{}) do
      {:ok, brand} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Corp.create_brand()

      brand
    end

    test "list_brands/0 returns all brands" do
      brand = brand_fixture()
      assert Corp.list_brands() == [brand]
    end

    test "get_brand!/1 returns the brand with given id" do
      brand = brand_fixture()
      assert Corp.get_brand!(brand.id) == brand
    end

    test "create_brand/1 with valid data creates a brand" do
      assert {:ok, %Brand{} = brand} = Corp.create_brand(@valid_attrs)
      assert brand.name == "some name"
      assert brand.parent_id == 42
    end

    test "create_brand/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Corp.create_brand(@invalid_attrs)
    end

    test "update_brand/2 with valid data updates the brand" do
      brand = brand_fixture()
      assert {:ok, brand} = Corp.update_brand(brand, @update_attrs)
      assert %Brand{} = brand
      assert brand.name == "some updated name"
      assert brand.parent_id == 43
    end

    test "update_brand/2 with invalid data returns error changeset" do
      brand = brand_fixture()
      assert {:error, %Ecto.Changeset{}} = Corp.update_brand(brand, @invalid_attrs)
      assert brand == Corp.get_brand!(brand.id)
    end

    test "delete_brand/1 deletes the brand" do
      brand = brand_fixture()
      assert {:ok, %Brand{}} = Corp.delete_brand(brand)
      assert_raise Ecto.NoResultsError, fn -> Corp.get_brand!(brand.id) end
    end

    test "change_brand/1 returns a brand changeset" do
      brand = brand_fixture()
      assert %Ecto.Changeset{} = Corp.change_brand(brand)
    end
  end
end
